/*
Andy Morgan
Auburn REU on SMART UAVs 2016
Research Director: Dr. Richard Chapman
*/

#include <SoftwareSerial.h>

// GPS Setup
#define rxGPS 3
#define txGPS 5
SoftwareSerial serialGPS = SoftwareSerial(rxGPS, txGPS);
String stringGPS = "";
char a;

void setup() {
  pinMode(rxGPS, INPUT);
  pinMode(txGPS, OUTPUT);


  Serial.begin(9600);
  Serial.println("Started");

  // GPS Setup
  serialGPS.begin(4800);
  digitalWrite(txGPS,HIGH);

  
  // Wait for GPS to become available
  while(!serialGPS.available()){
    Serial.println("Not available");
    delay (1000);
  }


  // Cut first gibberish
  while(serialGPS.available()){
    char a = serialGPS.read();
    Serial.println(a);
    if (a == '\r')
      break;
  }
  
  Serial.println("We got out of the break");
}

void loop()
{
  String s = checkGPS();
  Serial.println(s);
  if(s && s.substring(0, 6) == "$GPGGA")
  {
    Serial.println(s);
  }
}

// Check GPS and returns string if full line recorded, else false
String checkGPS()
{
  if (serialGPS.available())
  {
    char c = serialGPS.read();
    if (c != '\n' && c != '\r')
    {
      stringGPS  = (String)c;
    }
    else
    {
      if (stringGPS != "")
      {
        String tmp = stringGPS;
        stringGPS = "";
        return tmp;
      }
    }
  }
  return "";
}

