/*
Andy Morgan
Auburn REU on SMART UAVs 2016
Research Director: Dr. Richard Chapman
*/

#include <SoftwareSerial.h>

// GPS Setup
#define rxGPS 3
#define txGPS 5
#define INSIDE_FLIGHT_ZONE_SIZE 500 //this value is in meters



SoftwareSerial serialGPS = SoftwareSerial(rxGPS, txGPS);
String stringGPS = "";
char bufferIn;

//GPS Location Variables
double NLatitude;
double WLongitude;
double StartingNLatitude;
double StartingWLongitude;
double LatitudeThreshold;
double LongitudeThreshold;

void setup() {
  //Declare pins for GPS communication
  pinMode(rxGPS, INPUT);
  pinMode(txGPS, OUTPUT);

  //Serial Out Setup
  Serial.begin(9600);
  Serial.println("Started");

  // GPS Setup
  serialGPS.begin(38400);
  digitalWrite(txGPS,HIGH);
  
  // Wait for GPS to become available
  while(!serialGPS.available()){
    Serial.println("GPS not available");
    delay (1000);
    }
  Serial.println("GPS available");
  
  
  getGPSData(); 
  while(NLatitude == 0 && WLongitude == 0){
    getGPSData(); 
    //Serial.println(NLatitude);
  }
  Serial.println("GPS Calibrated");
  //Determine starting coordinates
  
  StartingNLatitude = NLatitude;
  StartingWLongitude = WLongitude;
  LatitudeThreshold = (INSIDE_FLIGHT_ZONE_SIZE/30.79) / 60; //There are 30.79 meters in a Lat minute, and 60 seconds in a minute
  LongitudeThreshold = (INSIDE_FLIGHT_ZONE_SIZE/24.39) / 60; //There are  meters in a Long minute, and 60 seconds in a minute

}

void loop()
{

  getGPSData();
  deployParachute();
  
}

// Get data from GPS signal according to the amount of data we need for each sequence
String fillGPSString(int sizeOfSequence, String GPSSequence)
{
for(int i=0; i<sizeOfSequence; i++){
       bufferIn = serialGPS.read();
       GPSSequence+= (String)bufferIn;
        }
return GPSSequence;
}



//Check GPS and see if we can grab data. If we can, fill string for use
void getGPSData (){
  // Find dollar sign '$' that will tell us beginning of GPS sequence
 stringGPS = ""; //clear string so that we can append
 if(serialGPS.available()){
    bufferIn = serialGPS.read();
    //Serial.println(a);
    }

//Grab beginnning sequence from GPS signal    
if( bufferIn =='$'){
    stringGPS= (String)bufferIn;
    for(int i=0; i<5; i++){
       bufferIn = serialGPS.read();
       stringGPS+= (String)bufferIn;
        }
    }

//We are only going to get data from this sequence
if( stringGPS == "$GPGGA"){
    Serial.println("Sequence: $GPGGA");
    stringGPS = fillGPSString( 74, stringGPS); //size of sequence for $GPGGA is 74 after beginning sequence
    NLatitude = stringGPS.substring(18,27).toFloat(); // This grabs the characters --the last number is the index (0 based +1)
    WLongitude = stringGPS.substring(30,40).toFloat(); // This grabs the characters --the last number is the index (0 based +1)
    Serial.print("Current Coodinates: ");Serial.print(NLatitude); Serial.print("    "); Serial.println(WLongitude);
  }
 
}

void deployParachute(){
//put code here
  
}



