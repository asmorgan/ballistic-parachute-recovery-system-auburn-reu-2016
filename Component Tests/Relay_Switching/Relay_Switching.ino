/* Andy Morgan
June 2016*/

/*-----( Import needed libraries )-----*/
/*-----( Declare Constants )-----*/
#define RELAY_ON 0
#define RELAY_OFF 1
/*-----( Declare objects )-----*/
/*-----( Declare Variables )-----*/
#define RELAY_PIN 5

void setup()   /****** SETUP: RUNS ONCE ******/
{
    
//-------( Initialize Pins so relays are inactive at reset)----
  digitalWrite(RELAY_PIN, RELAY_OFF);

//---( THEN set pins as outputs )----  
  pinMode(RELAY_PIN, OUTPUT);   
  
}//--(end setup )---


void loop()   /****** LOOP: RUNS CONSTANTLY ******/
{
  //Begin loop
  digitalWrite(RELAY_PIN, RELAY_ON);
  delay (5000);
  
  //Change lights
  digitalWrite(RELAY_PIN, RELAY_OFF);
  delay (5000);


}//--(end main loop )---



//*********( THE END )***********
