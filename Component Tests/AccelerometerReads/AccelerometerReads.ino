/*
 ADXL3xx

 Reads an Analog Devices ADXL3xx accelerometer and communicates the
 acceleration to the computer.  The pins used are designed to be easily
 compatible with the breakout boards from Sparkfun, available from:
 http://www.sparkfun.com/commerce/categories.php?c=80

 http://www.arduino.cc/en/Tutorial/ADXL3xx

 The circuit:
 analog 0: accelerometer self test
 analog 1: z-axis
 analog 2: y-axis
 analog 3: x-axis
 analog 4: ground
 analog 5: vcc

 created 2 Jul 2008
 by David A. Mellis
 modified 30 Aug 2011
 by Tom Igoe

 This example code is in the public domain.

*/
#include <Servo.h> 
// these constants describe the pins. They won't change:
const int groundpin = A5;             // analog input pin 4 -- ground
const int powerpin = A1;              // analog input pin 5 -- voltage
const int xpin = A2;                  // x-axis of the accelerometer
const int ypin = A3;                  // y-axis
const int zpin = A4;                  // z-axis (only on 3-axis models)
const int lowThresh = 340;
const int highThresh = 390;
int xval=0;
int yval=0;
int zval=0;




Servo myservo;

void setup() {
  // initialize the serial communications:
  Serial.begin(9600);

  // Provide ground and power by using the analog inputs as normal
  // digital pins.  This makes it possible to directly connect the
  // breakout board to the Arduino.  If you use the normal 5V and
  // GND pins on the Arduino, you can remove these lines.
  pinMode(groundpin, OUTPUT);
  pinMode(powerpin, OUTPUT);
  digitalWrite(groundpin, LOW);
  digitalWrite(powerpin, HIGH);
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);

  myservo.attach(9);
  myservo.write(80);  // set servo to close
  
}

void loop() {
  // print the sensor values:
  xval = analogRead(xpin);
  yval = analogRead(ypin);
  zval = analogRead(zpin);
  Serial.print(xval);
  // print a tab between values:
  Serial.print("\t");
  Serial.print(yval);
  // print a tab between values:
  Serial.print("\t");
  Serial.print(zval);
  if((xval<highThresh &&xval >lowThresh) &&  (yval<highThresh &&yval >lowThresh) && (zval<highThresh &&zval >lowThresh)) 
   { Serial.println("We are in freefall");
     myservo.write(0);
   }
  Serial.println();
  // delay before next reading:
  digitalWrite(2, LOW);
  digitalWrite(3, HIGH);
  delay(250);
  digitalWrite(2, HIGH);
  digitalWrite(3, LOW);
  delay(250);
}
