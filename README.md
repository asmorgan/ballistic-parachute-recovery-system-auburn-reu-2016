# README #

Andrew Morgan
Auburn University REU 2016
Research Advisor: Dr. Richard Chapman

All files associated with the Ballistic Parachute Recovery System are contained in this repository. The Arduino code is found on the root and other associated files/folders are also within this repo. All figures and test codes are shared. Enjoy!